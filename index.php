<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('<?php echo get_field('backgroundimage', 2); ?>')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>INTERVIEW</h1>
                        <span class="subheading">thanks for the opportunity</span>
                    </div>
                </div>
            </div>

        </div>
    </header>




<?php if ( have_posts() ): ?>
<div class="container">
    <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
    <div class="post-preview">

<?php while ( have_posts() ) : the_post();
?>

    <div class="post-preview">
        <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
            <h2 class="post-title">
                <?php the_title(); ?>
            </h2>
            <h3 class="post-subtitle">
                <?php $trimmed = wp_filter_nohtml_kses( wp_trim_words( get_the_content(), 25, "...." ) ); ?>
                <?php echo $trimmed; ?>

            </h3>
        </a>
        <p class="post-meta">Posted by
            <?php the_author_posts_link(); ?>
            on <time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time>
<?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
        </p>
    </div>
    <hr>
<?php endwhile; ?>
</div></div></div></div>
<?php else: ?>
<h2>No posts to display</h2>
<?php endif; ?>










<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>